all: hifiberry-dacplus-overlay.dts
	mkdir -p usr/lib/hifiberry-dkms
	dtc hifiberry-dacplus-overlay.dts > usr/lib/hifiberry-dkms/hifiberry-dacplus.dtbo
	dtc raspberry-overlay.dts > usr/lib/hifiberry-dkms/raspberry.dtbo

clean:
	rm -rf usr/lib/hifiberry-dkms
